import datetime as dt
import pandas as pd
from pathlib import Path
from multiprocessing.pool import Pool, ThreadPool

from airflow.models import DAG
from airflow.models.baseoperator import chain
from airflow.operators.python import PythonOperator
from airflow.providers.http.hooks.http import HttpHook
from airflow.providers.http.sensors.http import HttpSensor
from airflow.providers.snowflake.operators.snowflake import SnowflakeOperator

default_task_args = {
    'owner': 'kazanzhy',
    'start_date': dt.datetime(2021, 12, 1),
    'end_date': dt.datetime(2021, 12, 2),
    'depends_on_past': False,
    'retries': 0,
}

dag_args = {
    'schedule_interval': '0 4 * * *',
    'default_args': default_task_args,
    'catchup': True,
}

with DAG(dag_id='air_dag', **dag_args) as dag:

    dir_exists = HttpSensor(
        task_id='dir_exists',
        http_conn_id='adsb_hist_conn',
        endpoint='/{{ macros.ds_format(ds, "%Y-%m-%d", "%Y/%m/%d") }}/',
        response_check=lambda resp: resp.status_code == 200,
        poke_interval=60*1,  # 1 min
        mode='reschedule',
        timeout=60*60*1,  # 1 hour
    )

    def load_files_fn(path: str, folder: str):
        def loader(filename):
            resp = hook.run(f'{path}/{filename}')
            filepath = files_dir.joinpath(filename)
            filepath.write_bytes(resp.content)

        filenames = pd.date_range("00:00", "23:59", freq="5s").format(date_format='%H%M%SZ.json.gz')[:8]
        hook = HttpHook(method='GET', http_conn_id='adsb_hist_conn')
        files_dir = Path(f"/home/bucket/air/{folder}")
        files_dir.mkdir(parents=True, exist_ok=True)
        with ThreadPool(processes=4) as pool:
            pool.map(loader, filenames)
        #map(loader, filenames)

    load_files = PythonOperator(
        task_id='load_files',
        python_callable=load_files_fn,
        op_args=['{{ macros.ds_format(ds, "%Y-%m-%d", "%Y/%m/%d") }}', '{{ ds }}'],
    )

    sf_load_files_to_stage = SnowflakeOperator(
        task_id='sf_load_files_to_stage',
        sql='sql/sf_load_files_to_stage.sql',
        database='WAREHOUSE',
        schema='DWH',
        warehouse='X_SMALL_WH',
        role='ACCOUNTADMIN',
        snowflake_conn_id='snowflake_conn',
    )

    sf_copy_into_table = SnowflakeOperator(
        task_id='sf_copy_into_table',
        sql='sql/sf_copy_files_to_table.sql',
        database='WAREHOUSE',
        schema='DWH',
        warehouse='X_SMALL_WH',
        role='ACCOUNTADMIN',
        snowflake_conn_id='snowflake_conn',
    )

    chain(
        load_files,
        sf_load_files_to_stage,
        sf_copy_into_table
    )
