CREATE STAGE IF NOT EXISTS DWH.public.stage;

PUT file:///home/bucket/air/{{ ds }}/*Z.json.gz @DWH.public.stage/bucket/air/{{ ds }}
    OVERWRITE = TRUE;