CREATE OR REPLACE TABLE DWH.public.air_{{ ds_nodash }} (
    data VARIANT
);

COPY INTO DWH.public.air_{{ ds_nodash }}
    FROM @DWH.public.stage/bucket/air/{{ ds }}/*Z.json.gz
    FILE_FORMAT = ( TYPE = JSON );