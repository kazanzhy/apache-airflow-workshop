import datetime as dt
from pathlib import Path

from airflow.models import DAG
from airflow.models.baseoperator import chain
from airflow.operators.python import PythonOperator
from airflow.providers.http.sensors.http import HttpSensor
from airflow.providers.http.operators.http import SimpleHttpOperator
from airflow.providers.snowflake.operators.snowflake import SnowflakeOperator
from airflow.providers.postgres.operators.postgres import PostgresOperator

default_task_args = {
    'owner': 'kazanzhy',
    'start_date': dt.datetime(2020, 2, 11),
    'depends_on_past': False,
    'retries': 0,
    'email': [],
}

dag_args = {
    'schedule_interval': None, #'*/9 * * * *',
    'default_args': default_task_args,
}

with DAG(dag_id='titanic_dag', **dag_args) as dag:

    titanic_file_exists = HttpSensor(
        task_id='titanic_file_exists',
        http_conn_id='stanford_http_conn',
        endpoint='/class/archive/cs/cs109/cs109.1166/stuff/titanic.csv',
        response_check=lambda resp: resp.status_code == 200,
        poke_interval=60*1,  # 1 min
        mode='reschedule',
        timeout=60*60*1,  # 1 hour
    )

    download_text = SimpleHttpOperator(
        task_id='download_text',
        http_conn_id='stanford_http_conn',
        endpoint='/class/archive/cs/cs109/cs109.1166/stuff/titanic.csv',
        method='GET',
    )

    def save_raw_to_file(filename, ti, **context):
        text = ti.xcom_pull(task_ids='download_text', key='return_value')
        Path(filename).write_text(text)
    save_raw_to_file = PythonOperator(
        task_id='save_raw_to_file',
        python_callable=save_raw_to_file,
        op_args=['/home/bucket/titanic_raw.csv'],
        provide_context=True,
    )

    pg_load_into_table = PostgresOperator(
        task_id='pg_load_into_table',
        sql='sql/pg_copy_into_table.sql',
        postgres_conn_id='dwh_conn',
    )

    pg_transform = PostgresOperator(
        task_id='pg_transform',
        sql='sql/pg_transform.sql',
        postgres_conn_id='dwh_conn',
    )

    sf_load_file_to_stage = SnowflakeOperator(
        task_id='sf_load_file_to_stage',
        sql='sql/sf_load_file_to_stage.sql',
        database='WAREHOUSE',
        schema='DWH',
        warehouse='X_SMALL_WH',
        role='ACCOUNTADMIN',
        snowflake_conn_id='snowflake_conn',
    )

    sf_copy_into_table = SnowflakeOperator(
        task_id='sf_copy_into_table',
        sql='sql/sf_copy_into_table.sql',
        database='WAREHOUSE',
        schema='DWH',
        warehouse='X_SMALL_WH',
        role='ACCOUNTADMIN',
        snowflake_conn_id='snowflake_conn',
    )

    sf_transform = SnowflakeOperator(
        task_id='sf_transform',
        sql='sql/sf_transform.sql',
        database='WAREHOUSE',
        schema='DWH',
        warehouse='X_SMALL_WH',
        role='ACCOUNTADMIN',
        snowflake_conn_id='snowflake_conn',
    )

    # titanic_file_exists.set_downstream(download_text) # titanic_file_exists >> download_text

    #titanic_file_exists >> download_text >> save_raw_to_file >> pg_load_into_table >> pg_transform
    #save_raw_to_file >> sf_load_file_to_stage >> sf_copy_into_table >> sf_transform

    chain(
        titanic_file_exists,
        download_text,
        save_raw_to_file,
        pg_load_into_table,
        pg_transform
    )

    chain(
        save_raw_to_file,
        sf_load_file_to_stage,
        sf_copy_into_table,
        sf_transform
    )