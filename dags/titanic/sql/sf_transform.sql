CREATE OR REPLACE TABLE DWH.public.titanic_stats AS
    SELECT pclass, sex,
        count_if(survived = 1) as survived_cnt,
        count_if(survived = 0) as not_survived_cnt,
        count(*) as total_cnt
    FROM DWH.public.titanic
    GROUP BY 1, 2
    ORDER BY 1, 2;