DROP TABLE IF EXISTS public.titanic;

CREATE TABLE public.titanic (
    survived INT,
    pclass INT,
    name VARCHAR,
    sex VARCHAR,
    age DECIMAL,
    siblings_spouses_aboard INT,
    parents_children_aboard INT,
    fare DECIMAL
);

COPY public.titanic
FROM '/home/bucket/titanic_raw.csv'
WITH (FORMAT csv, HEADER true);