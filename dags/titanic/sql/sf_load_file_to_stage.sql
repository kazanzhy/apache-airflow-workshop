CREATE STAGE IF NOT EXISTS DWH.public.stage;

PUT file:///home/bucket/titanic_raw.csv @DWH.public.stage/bucket
    OVERWRITE = TRUE;