CREATE OR REPLACE TABLE DWH.public.titanic (
    survived INT,
    pclass INT,
    name VARCHAR,
    sex VARCHAR,
    age INT,
    siblings_spouses_aboard INT,
    parents_children_aboard INT,
    fare DECIMAL
);

COPY INTO DWH.public.titanic
    FROM @DWH.public.stage/bucket/titanic_raw.csv
    FILE_FORMAT = ( TYPE = CSV SKIP_HEADER = 1 );