DROP TABLE IF EXISTS public.titanic_stats;

CREATE TABLE public.titanic_stats AS
    SELECT pclass, sex,
        sum((survived = 1)::int) as survived_cnt,
        sum((survived = 0)::int) as not_survived_cnt,
        count(*) as total_cnt
    FROM DWH.public.titanic
    GROUP BY 1, 2
    ORDER BY 1, 2;