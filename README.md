## Run Apache Airflow locally

### Using Docker Compose
Official [docker-compose.yaml](https://github.com/apache/airflow/blob/main/docs/apache-airflow/start/docker-compose.yaml)
```shell
docker-compose up
```

### Using Astronomer CLI
```shell
curl -sSL https://install.astronomer.io | sudo bash;
# astro dev init;
astro dev start;
astro dev stop;
```

### Using single Docker container:
```shell
# git clone git@github.com:kazanzhy/dockers.git
# cd dockers/airflow  
# gedit airflow.cfg
# docker build -t kazanzhy/airflow .
docker pull kazanzhy/airflow:latest
docker run -d -p 8080:8080 --name airflow_app -v $(pwd)/dags:/home/airflow/dags kazanzhy/airflow
```
### Airflow UI
Follow http://localhost:8080  
Login: `admin`  
Password: `admin`

## DAG authoring
```shell
python3 -m pip install --upgrade pip
python3 -m pip install -r requirements.txt
```

## Connections
**stanford_http_conn**  
type: `http`  
host: `web.stanford.edu`  
schema: `https`

**dwh_conn**  
type: `postgres`  
host: `data-warehouse`  
login: `admin`  
password: `admin`  
schema: `dwh`

**snowflake_conn**  
type: `snowflake`  
host: `https://xy77777.eu-west-2.aws.snowflakecomputing.com` # Your Snowflake  
login: `admin`  
password: `` Snowflake pass  
schema: `dwh`

**adsb_hist_conn**
type: `http`  
host: `samples.adsbexchange.com/readsb-hist`  
schema: `https`

## Links
- Apache Airflow repo: https://github.com/apache/airflow;
- My image: https://github.com/kazanzhy/dockers/tree/main/airflow;
- Astronomer CLI: https://www.astronomer.io/docs/cloud/stable/develop/cli-quickstart 
- Slides: https://docs.google.com/presentation/d/1k0JG942JoWj4QmCqlsufpKV5keEZrxVyOpFP9qXXtn0/edit?usp=sharing
